const UserService = require('../app/services/user-service');

const userService = new UserService();

describe('UserService', () => {
  describe('getAttributeFromAllUsers', () => {
    test('should return website', () => {
      const users = [
        { name: 'Peter', website: 'www.peter.com' },
        { name: 'Joane', website: 'www.joane.com' },
      ];
      const expectedResult = [
        'www.peter.com',
        'www.joane.com',
      ];
      const receivedResult = userService.getAttributeFromAllUsers(users, 'website');
      expect(expectedResult).toStrictEqual(receivedResult);
    });

    test('should return empty if found not website', () => {
      const users = [
        { name: 'Peter' },
        { name: 'Joane' },
      ];
      const expectedResult = [];
      const receivedResult = userService.getAttributeFromAllUsers(users, 'website');
      expect(expectedResult).toStrictEqual(receivedResult);
    });
  });

  describe('getSortedAttributesFromUsers', () => {
    test('should return sorted name, email and company from users', () => {
      const users = [
        {
          name: 'Bob', email: 'bob@mutant.com', company: { name: 'Bob S.A' }, website: 'www.bob.com',
        },
        {
          name: 'Alice', email: 'alice@mutant.com', company: { name: 'Alice S.A' }, website: 'www.alice.com',
        },
      ];
      const expectedResult = [
        { name: 'Alice', email: 'alice@mutant.com', company: { name: 'Alice S.A' } },
        { name: 'Bob', email: 'bob@mutant.com', company: { name: 'Bob S.A' } },
      ];
      const attributes = ['name', 'email', 'company'];
      const receivedResult = userService
        .getSortedAttributesFromUsers(users, 'name', attributes);
      expect(expectedResult).toStrictEqual(receivedResult);
    });
  });

  describe('getUserByAddressSuiteFilter', () => {
    test('should return users that has Suite in address name using lowercase', () => {
      const users = [
        {
          name: 'Bob', email: 'bob@mutant.com', address: { street: '17th Bob, 54', suite: 'Apt 45' }, website: 'www.bob.com',
        },
        {
          name: 'Alice', email: 'alice@mutant.com', address: { street: '21th Alice, 98', suite: 'Suite 22' }, website: 'www.alice.com',
        },
      ];

      const expectedResult = [
        {
          name: 'Alice', email: 'alice@mutant.com', address: { street: '21th Alice, 98', suite: 'Suite 22' }, website: 'www.alice.com',
        },
      ];
      const receivedResult = userService.getUserByAddressSuiteFilter(users, 'Suite');
      expect(expectedResult).toStrictEqual(receivedResult);
    });
    test('should return users that has Suite in address name using uppercase', () => {
      const users = [
        {
          name: 'Bob', email: 'bob@mutant.com', address: { street: '17th Bob, 54', suite: 'Apt 45' }, website: 'www.bob.com',
        },
        {
          name: 'Alice', email: 'alice@mutant.com', address: { street: '21th Alice, 98', suite: 'Suite 22' }, website: 'www.alice.com',
        },
      ];

      const expectedResult = [
        {
          name: 'Alice', email: 'alice@mutant.com', address: { street: '21th Alice, 98', suite: 'Suite 22' }, website: 'www.alice.com',
        },
      ];
      const receivedResult = userService
        .getUserByAddressSuiteFilter(users, 'SUITE');
      expect(expectedResult).toStrictEqual(receivedResult);
    });
    test('should return empty if search for user that has Suite in address name has no result', () => {
      const users = [
        {
          name: 'Bob', email: 'bob@mutant.com', address: { street: '17th Bob, 54', suite: 'Apt 45' }, website: 'www.bob.com',
        },
        {
          name: 'Alice', email: 'alice@mutant.com', address: { street: '21th Alice, 98', suite: 'Suite 22' }, website: 'www.alice.com',
        },
      ];
      const expectedResult = [];
      const receivedResult = userService
        .getUserByAddressSuiteFilter(users, 'Apartment');
      expect(expectedResult).toStrictEqual(receivedResult);
    });
  });
});

