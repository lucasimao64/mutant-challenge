const {
  pick,
  map,
  sortWith,
  ascend,
  pipe,
  prop,
  filter,
} = require('ramda');

const got = require('got');

class UserService {
  async getUsers () {
    const apiResult = await got(process.env.DATA_URL).json();
    return apiResult;
  }

  getSortedAttributesFromUsers (users, sortedBy, attributes) {
    const sortRule = [ascend(prop(sortedBy))];
    const getAttributes = pick(attributes);
    const getSortedNameAndEmailFromUser = pipe(
      map(getAttributes),
      sortWith(sortRule)
    );
    return getSortedNameAndEmailFromUser(users);
  }

  getAttributeFromAllUsers (users, attribute) {
    const getAttributeValue = (user) => user[attribute];
    const getAttributeFromUsers = pipe(
      filter(getAttributeValue),
      map(getAttributeValue)
    );
    return getAttributeFromUsers(users);
  }

  verifyIfStringContains (stringThatContains, stringThatIsContained) {
    const containsNormalized = stringThatContains.toUpperCase();
    const containedNormalized = stringThatIsContained.toUpperCase();
    return containsNormalized.includes(containedNormalized);
  }

  getUserByAddressSuiteFilter (users, attributeFilter) {
    return users.filter((user) => this.verifyIfStringContains(
      user.address.suite,
      attributeFilter
    ));
  }
}
module.exports = UserService;
