const UserService = require('../services/user-service');

const userService = new UserService();

class UserController {
  static routes () {
    return {
      websites: '/users/websites',
      list: '/users',
      address: '/users/address',
    };
  }

  async getWebsitesFromUsers () {
    return async (req, res) => {
      const users = await userService.getUsers();
      res.send(userService.getAttributeFromAllUsers(users, 'website'));
    };
  }

  async getSortedAttributesFromUsers () {
    return async (req, res) => {
      const users = await userService.getUsers();
      const attributes = ['name', 'email', 'company'];
      res.send(userService.getSortedAttributesFromUsers(users, 'name', attributes));
    };
  }

  async getUserWithAddressThatHasSuite () {
    return async (req, res) => {
      const users = await userService.getUsers();
      res.send(userService.getUserByAddressSuiteFilter(users, 'Suite'));
    };
  }
}

module.exports = UserController;
