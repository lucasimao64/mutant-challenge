
const UserController = require('../controllers/user-controller');
const logger = require('../../config/logger');
const { notFoundMiddleware } = require('./middlewares/middlewares');

const userController = new UserController();

module.exports = async (app) => {
  const userRoutes = UserController.routes();

  app.use((req, resp, next) => {
    logger.info(`Request ${req.method} to ${req.originalUrl}`);
    next();
  });

  app.get(userRoutes.websites, await userController.getWebsitesFromUsers());

  app.get(
    userRoutes.list,
    await userController.getSortedAttributesFromUsers()
  );

  app.get(
    userRoutes.address,
    await userController.getUserWithAddressThatHasSuite()
  );

  app.use(notFoundMiddleware);
};

