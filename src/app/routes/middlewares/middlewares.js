const logger = require('../../../config/logger');

const notFoundMiddleware = (req, res) => {
  res.status(404).send({ status: '404', message: 'Not Found' });
  logger.error(`Response: ${res.statusCode}`);
};
module.exports = { notFoundMiddleware };
