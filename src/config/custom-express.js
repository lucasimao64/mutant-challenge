const express = require('express');

const routes = require('../app/routes/routes.js');

const app = express();

routes(app);

module.exports = app;
