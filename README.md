# Desafio Mutant
##### Lucas Costa

Este projeto é uma API criada como Desafio do Processo Seletivo da empresa [Mutant](https://mutantbr.com/), ele utiliza os dados salvos em https://jsonplaceholder.typicode.com/users para prover uma API que realiza as seguintes operações:

- Busca os websites de todos os usuários
- Retorna, de todos os usuários o Nome, email e a empresa em que trabalha (em ordem alfabética).
- Retorna todos os usuários que no endereço contem a palavra `suite`.
- Salva os logs das operações no Elasticsearch.

# Sumário
- [Requisitos](#Requisitos)
- [Instalação](#Instalação)
- [Tecnologias](#Tecnologias)
- [Observações](#Observações)
- [Referências](#Referências)

# Requisitos
- [Node.js](https://nodejs.org/) >= 12.x (Opcional)
- [Docker](https://www.docker.com/) >= 19.x
- [Docker Compose](https://docs.docker.com/compose/) >= 1.26.x

# Instalação
## Localmente

Se você deseja executar o node em sua máquina local, use:
```sh
$ git clone https://lucasimao64@bitbucket.org/lucasimao64/mutant-challenge.git
$ cd mutant-challenge
$ npm install
$ npm run dev
```

Para iniciar:
```sh
$ npm start
```

## Docker
Clone o repositório e rode:
```sh
$ git clone https://lucasimao64@bitbucket.org/lucasimao64/mutant-challenge.git
$ cd mutant-challenge
$ docker-compose up --build -d
```

## Testes
Para executar os testes, você deve ter o node instalado em seu ambiente e executar:
```sh
$ npm test
```
## Lint
Para executar o  eslint no projeto, você deve ter o node instalado em seu ambiente e executar:
```sh
$ npm run lint
```

## Elasticsearch
Para visualizar os logs gerados no elasticsearch, acesse: [Kibana](http://localhost:5601/app/kibana#/discover) e pesquise com 'log-*' como filtro.
OBS: O docker espera o elasticsearch estar disponível antes de iniciar o kibana, logo pode ocorrer do link demorar alguns minutos para estar disponível após [iniciar o docker](#Docker). 

# Tecnologias
Para a criação do projeto, foram utilizadas as seguintes tecnologias, text editor and packages from [npm](https://www.npmjs.com/):

* [Node.js](https://nodejs.org/)
* [Visual Studio Code](https://code.visualstudio.com/)- editor de texto com os seguintes pluging instalados: [DotENV](https://github.com/mikestead/vscode-dotenv), [ESLint](https://github.com/Microsoft/vscode-eslint), [GitLens](https://github.com/eamodio/vscode-gitlens) e [vscode-icons](https://github.com/vscode-icons/vscode-icons).
* [Express](https://github.com/expressjs/express) - Framework para node.
* [Jest](https://jestjs.io/) - Framework para testes em js
* [Winston](https://github.com/winstonjs/winston) - Biblioteca para logs.
* [Ramda](https://ramdajs.com/) - Para realizar as consultas.
* [ESLint](https://github.com/eslint/eslint) - ESLint para padronização de código.
* [Docker Compose](https://docs.docker.com/compose/) -Ferramenta para criação dos containeres que rodam o projeto.
* [Elasticsearch](https://www.elastic.co/pt/) - Ferramenta para persistência de dados e buscas rápidas (Dos logs)
* [Kibana](https://www.elastic.co/pt/kibana) - Ferramenta para a visualização dos dados no Elasticsearch


# Observações
- Os websites de todos os usuários
  - Foi implementada uma requisição do tipo GET na rota `localhost:8080/users/websites`
- O Nome, email e a empresa em que trabalha (em ordem alfabética).
  - Foi implementada uma requisição do tipo GET na rota `localhost:8080/users`
- Mostrar todos os usuários que no endereço contem a palavra `suite`
  - Foi implementada uma requisição do tipo GET na rota `localhost:8080/users/address`
  - Obs: A interpretação deste item foi que: Deve-se buscar por usuários que possuam suite em seu endereço. Analisando os dados, foi identificado o atributo `suite`, que podia possuir `"Suite N"` ou `"Apt N"`, logo, entendi que o filtro deve se tratar dessa forma. Então, ele vai buscar todos os usuários que possuem o texto `"Suite"` como dado dentro da chave `suite`
- Salvar logs de todas interações no elasticsearch
  - Foi criada uma instância do elastic search no docker. Para visualizar os logs gerados, acesse o link do Kibana e utilize o filtro `"log-*"`
- EXTRA: Criar test unitário para validar os itens a cima.
  - Foram criados 6 testes unitários em src/tests/user-service.test.js
  - Para executá-los, use `npm test` no terminal.

- Entregar o aplicativo usando o Docker e garantir que ele &quot;sobreviva&quot; nas reinicializações
  - A aplicação foi implementada para rodar no [docker compose](#docker).
  - Para executá-la use: `docker-compose up --build -d`
  - OBS: A Api e o Kibana deverão aguardar o Elasticsearch iniciar antes de rodar. Porém, o Kibana tem um tempo de inicialização um pouco mais longo, logo, pode ocorrer da api estar disponível antes do Kibana (mas os logs serão enviados normalmente)

# Referências
- [Node.js Best Pratices](https://github.com/i0natan/nodebestpractices)
- [Alura Courses](https://www.alura.com.br/)
- [Structured logging in Node.js with Winston and Elasticsearch](https://www.thedreaming.org/2020/06/24/structured-logging-nodejs/)
- [Running Elasticsearch v7.1.1 in Docker containers](https://github.com/justmeandopensource/elk/tree/master/docker)