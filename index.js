require('dotenv').config();

const app = require('./src/config/custom-express');
const logger = require('./src/config/logger');

const port = 8080;

app.listen(port, () => {
  logger.info(`Server is running on port ${port}`);
});
